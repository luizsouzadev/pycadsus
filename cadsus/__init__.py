import logging
import datetime
from suds.client import Client,TransportError
from suds.wsse import Security, UsernameToken
from suds import WebFault
from suds.sax.date import Date
import sys
logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.DEBUG)

class Conexao:
    wsdl_hm = "https://servicoshm.saude.gov.br/cadsus/CadsusService/v5r0?wsdl"
    wsdl_prod =  "https://servicos.saude.gov.br/cadsus/CadsusService/v5r0?wsdl"

    __user_token = "CADSUS.CNS.PDQ.PUBLICO"
    __pass_token = "kUXNmiiii#RDdlOELdoe00966"

    def __init__(self, producao=False):
        self.producao = producao

    def setToken(self, usertoken, passtoken):
        self.__user_token = usertoken
        self.__pass_token = passtoken

    def conectar(self,userToken = __user_token, passToken = __pass_token):
        usernameToken = UsernameToken(userToken,passToken)

        usernameToken.setnonce()
        usernameToken.setcreated()

        if not(self.producao):
            self.client = Client(self.wsdl_hm, faults=True, cache=None)
        else:
            self.client = Client(self.wsdl_prod)

        security = Security()
        security.tokens = [usernameToken]

        self.client.set_options(wsse=security)
        self.client.set_options(service="CadsusService")
        self.client.set_options(port="CadsusServicePort")

class Pesquisa:

    __options_wsdl = {"nomeCompleto":"ns33:NomeCompleto",
            "Mae":"ns33:NomeCompleto",
            "dataNascimento":Date,
            "FiltroPesquisa": "ns45:FiltroPesquisa",
            "CPF": "ns19:CPF",
            "UF": "ns",
            "CNESUsuario": "ns44:CNESUsuario",
            "CNS":"ns2:CNS"}

    __filtro = {}
    __filtro_pesquisa = None

    __usuario_cnes = None

    def __init__(self,conexao = None, **auth):
        try:
            #verifica se foi informado as chaves para conexão ao CADSUS
            if not(all(coll in auth for coll in ("cnes","usuario", "senha"))):
                raise ValueError("Necessario informar o cnes, usuario e senha de acesso ao CadSus.")
            #verifica se nos valores informados tem None ou é empty
            for k,v in auth.items():
                if v in ("", None):
                    raise ValueError("Valor '"+str(v)+"' inválido para "+str(k))
            if conexao is None:
                self.conexao = Conexao()
            else:
                type(conexao)
                if isinstance(conexao,Conexao):
                    self.conexao = conexao
                else:
                    raise ValueError("conexao deve ser uma instancia de Conexao")

            self.cnes = auth['cnes']
            self.usuario = auth['usuario']
            self.senha = auth['senha']

            if "filtro" in auth:
                self.__filtro = auth['filtro']

            self.conexao.conectar()
        except ValueError as e:
            print(e)
            return None

    def __create_obj_xsd(self,key, *value , **values):
        tipo = self.__options_wsdl[key]
        if isinstance(tipo,str):
            tipo_filtro = self.conexao.client.factory.create(tipo)
            attrs = tipo_filtro['__keylist__']
            if len(attrs) == 1:
                attr = attrs[0]
                tipo_filtro[attr] = value[0]
            else:
                for i,attr in enumerate(attrs):
                    tipo_filtro[attr] = values.get(attr)
        else:
            tipo_filtro = tipo(value[0])

        return tipo_filtro


    def __create_filter(self,filtro=None):
        if self.__filtro_pesquisa is not None and filtro is None:
            return self.__filtro_pesquisa

        if filtro is None:
            filtro = self.__filtro

        filtro_pesquisa = self.conexao.client.factory.create(self.__options_wsdl["FiltroPesquisa"])

        for k,v in filtro.items():
            if v is not None:
                tipo_filtro = self.__create_obj_xsd(k,v)
                filtro_pesquisa[k] = tipo_filtro

        filtro_pesquisa.tipoPesquisa = "APROXIMADA"
        return filtro_pesquisa

    def set_filtro(self,chave,valor):
        self.__filtro[chave] = valor

    def __extract_value(self,obj):
        #até esta versão somente estava sendo retorno tipo XSD, boolean ou date
        if isinstance(obj, datetime.date) or isinstance(obj, bool):
            return obj

        if type(obj).__name__ == 'Text' or obj is None:
            return obj

        attrs = obj['__keylist__']
        if len(attrs) == 1:
            value = obj[attrs[0]]
            return value
        else:
            value = {}
            for attr in attrs:
                #irá remover o Type do fim do nome do tipo
                value[attr] = self.__extract_value(obj[attr])
            return value

    def __process_result(self, data):
        result = list()
        for row in data:
            attrs = row['__keylist__']
            element = {}
            for i,attr in enumerate(attrs):
                element[attr] = self.__extract_value(row[attr])
            result.append(element)
        return result

    def executar(self,filtro = None):
        try:
            if (filtro is None or filtro == {}) and self.__filtro == {}:
                raise ValueError("Informe um filtro para pesquisa.")

            if self.__usuario_cnes is None:
                self.__usuario_cnes = self.__create_obj_xsd("CNESUsuario",CNES=self.cnes, Usuario=self.usuario, Senha=self.senha)

            client = self.conexao.client
            #caso seja uma consulta de CNS
            if 'CNS' in filtro:
                print("TESTE")
                cns = self.__create_obj_xsd('CNS', numeroCNS = filtro['CNS'])
                result = client.service.consultar(CNESUsuario = self.__usuario_cnes,CNS = cns)
            #criar o filtro a ser pesquisado
            else:
                self.__filtro_pesquisa = self.__create_filter(filtro)
                result = client.service.pesquisar(CNESUsuario = self.__usuario_cnes, FiltroPesquisa = self.__filtro_pesquisa, higienizar = True)

            return self.__process_result(result)

        except ValueError as e:
            return {'Error': e.message, 'e':e}
        except WebFault as e:
            if "Mensagem" in e.fault.detail.MSFalha:
                return {'Error': e.fault.detail.MSFalha.Mensagem.descricao, 'Code':e.fault.detail.MSFalha.Mensagem.codigo}
            else:
                return {'Error': e, 'Code': 'NAO TRATADO'}
        except ConnectionResetError as e:
            return {'Error': e, 'Code': e}
        except Exception as e:
            # print(type(sys.exc_info()[1]))
            # return {'Error':'CNES, Usuário ou Senha inválidos! Senha não pode ser None ou vazia', 'Code': '500'}
            return {'Error': e, 'Code':'NAO TRATADO'}
